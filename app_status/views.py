from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Message_Form
from .models import Status
# Create your views here.
response = {}

def index(request):
    html = 'app_status/app_status.html'
    #TODO Implement, isilah dengan 6 kata yang mendeskripsikan anda
    response['author'] = "Farhan"
    status = Status.objects.all().order_by('-created_date')
    response['status'] = status
    html = 'app_status/app_status.html'
    response['message_form'] = Message_Form
    return render(request, html, response)

def add_status(request):
    form = Message_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['message'] = request.POST['message']
        status = Status(message=response['message'])
        status.save()
        html ='app_status/app_status.html'
        render(request, html, response)
        return HttpResponseRedirect('/app_status/')	
    else:        
        return HttpResponseRedirect('/add_status/')	