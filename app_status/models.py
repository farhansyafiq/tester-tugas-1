from django.db import models

# Create your models here.
class Status(models.Model):
	message = models.CharField(max_length=100)
	created_date = models.DateTimeField(auto_now_add=True)