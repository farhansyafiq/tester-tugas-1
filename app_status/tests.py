from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, add_status
from .models import Status
from .forms import Message_Form
from django.utils import timezone
# Create your tests here.

class AppStatusUnitTest(TestCase):

    def test_app_status_url_is_exist(self):
        response = Client().get('/app_status/')
        self.assertEqual(response.status_code, 200)

    def test_app_status_using_index_func(self):
        found = resolve('/app_status/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_status(self):
        # Creating a new activity
        new_activity = Status.objects.create(message="TESTCASEMODEL", created_date=timezone.now())

        # Retrieving all available activity
        counting_all_available_status = Status.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)

    def test_model_can_create_new_activity(self):
            #Creating a new activity
        new_activity = Status.objects.create(created_date=timezone.now(), message='Aku mau latihan ngoding deh')

            #Retrieving all available activity
        counting_all_available_activity = Status.objects.all().count()
        self.assertEqual(counting_all_available_activity,1)

    def test_app_status_post_success_and_render_the_result(self):
        response_post = Client().post('/app_status/add_status', {'message': 'test'})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/app_status/')
        html_response = response.content.decode('utf8')
        self.assertIn('test', html_response)

    def test_app_status_error_and_render_the_result(self):
        response_post = Client().post('/app_status/add_status', {'message': ''})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/app_status/')
        html_response = response.content.decode('utf8')
        self.assertNotIn('test', html_response)
